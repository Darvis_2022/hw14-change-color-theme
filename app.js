const buttonTheme = document.querySelector(".theme-btn");
const currentThemePage = localStorage.getItem("theme");

buttonTheme.addEventListener("click", () => {
    
  document.body.classList.toggle("dark-theme");

  let themeContent = "light";

  if (document.body.classList.contains("dark-theme")) {
    themeContent = "dark";
  }

  localStorage.setItem("theme", themeContent);
});

if (currentThemePage == "dark") {
    document.body.classList.add("dark-theme");
}